package id.ac.ub.room.recyclernya.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import id.ac.ub.room.R;
import id.ac.ub.room.recyclernya.model.Mahasiswa;
import id.ac.ub.room.recyclernya.viewholder.MahasiswaViewHolder;

public class MahasiswaAdapter extends RecyclerView.Adapter<MahasiswaViewHolder>{

    private final ArrayList<Mahasiswa> mahasiswas;
    private Context context;

    public MahasiswaAdapter(ArrayList<Mahasiswa> mahasiswas, Context context) {
        this.mahasiswas = mahasiswas;
        this.context = context;
    }

    @NonNull
    @Override
    public MahasiswaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
        return new MahasiswaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MahasiswaViewHolder holder, int position) {
        holder.getTv_nama().setText(mahasiswas.get(position).getNama());
        holder.getTv_nim().setText(mahasiswas.get(position).getNim());
        Glide.with(context).load(mahasiswas.get(position).getIdFoto()).into(holder.getIv_foto());
    }

    @Override
    public int getItemCount() {
        return mahasiswas.size();
    }

}
